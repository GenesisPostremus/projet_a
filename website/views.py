from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader, context
from django.template.loader import get_template


def index(request):
    return render(request, 'website/index.html')


def images(request, slug):
    return render(request, 'website/images.html', {"post": slug})


def presentation(request):
    return render(request, 'website/presentation.html')


def categorie(request):
    return render(request, 'website/categorie.html')