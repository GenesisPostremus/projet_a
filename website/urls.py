from django.urls import path

from . import views

app_name = 'website'

urlpatterns = [
    path('', views.index, name='index'),
    path('categorie/', views.categorie, name='images'),
    path('images/<slug:slug>/', views.images, name='images'),
    path('presentation/', views.presentation, name='presentation'),
]
